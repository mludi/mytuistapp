import ProjectDescription
import ProjectDescriptionHelpers
import MyPlugin


// MARK: - Project

// Local plugin loaded
let localHelper = LocalHelper(name: "MyPlugin")

let infoPlist: [String: InfoPlist.Value] = [
    "CFBundleShortVersionString": "1.0",
    "CFBundleVersion": "1",
    "UIMainStoryboardFile": "",
    "UILaunchStoryboardName": "LaunchScreen"
]

let project = Project(
    name: "MyTuistApp",
    settings: .settings(
        configurations: [
            .release(
                name: "Release",
                xcconfig: "Configs/Base.xcconfig"
            ),
            .debug(
                name: "Debug",
                xcconfig: "Configs/Base.xcconfig"
            )
        ]
    ),
    targets: [
        Target(
            name: "MyTuistApp",
            platform: .iOS,
            product: .app,
            bundleId: "$(PRODUCT_BUNDLE_IDENTIFIER)",
            infoPlist: .extendingDefault(with: infoPlist),
            sources: ["Targets/MyTuistApp/Sources/**"],
            resources: ["Targets/MyTuistApp/Resources/**"],
            settings: .settings(
                configurations: [
                    .debug(
                        name: "Debug",
                        xcconfig: "Configs/MyTuistApp/Client-Debug.xcconfig"
                    ),
                    .release(
                        name: "Release",
                        xcconfig: "Configs/MyTuistApp/Client-Release.xcconfig"
                    )
                ]
            )
        )
    ]
)
